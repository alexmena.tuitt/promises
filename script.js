console.log("test");

console.log("test 2");

// Promise object
// 1. waiting for something to happen
// e-evaluate and fire the next line code
	// reject
	// resolve


	let myPromise = new Promise( function(resolve,reject){
		let x = 0;

		if (x == 0) {
			resolve({ msg : "success" })
		} else {
			reject("rejected")
		}
	})

	// console.log(myPromise)

	// myPromise
	// .then( (result) => {
	// 	console.log(result)
	// 	function tester() {
	// 		console.log("hello world")
	// 	}
	// 	tester()
	// })
	// .catch((err)=>{
	// 	console.log( err )
	// })

	// other way of catching reject and resolve (without catch())
	// myPromise
	// .then( (success) =>{
	// 	console.log(success)
	// }, (err) => {
	// 	console.log(err)
	// })


async function test(){
	let result = await myPromise;
	console.log(result)
}
console.log("log from bottom of page")

	